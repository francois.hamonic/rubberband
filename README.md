requires base packages build-essential git gcc-9 g++-9 
and cmake gfortran for compiling the thirdparties

requires Clp, Lemon:

from package manager :
    sudo apt install libeigen3-dev
    sudo apt install coinor-libclp-dev coinor-libcbc-dev
    sudo apt install liblemon-dev
    
I recommend getting librairies from sources for lattest versions : 

Clp : using coinbrew :
    mkdir coinor
    cd coinor
    git clone https://github.com/coin-or/coinbrew
    export OPT_CXXFLAGS="-pipe -flto -O3 -march=native"
    export OPT_CFLAGS="-pipe -flto -O3 -march=native"
    export LDFLAGS="-pipe -flto"
    ./coinbrew/coinbrew build Clp:releases/1.17.5
    add 'export LD_LIBRARY_PATH=/home/plaiseek/Libs/coinor/dist/lib' to your ~/.bashrc
        
Lemon : http://lemon.cs.elte.hu/trac/lemon/wiki/Downloads
    cd lemon-x.y.z
    mkdir build
    cd build
    cmake ..
    make
    sudo make install

Then specify the paths in the makefile