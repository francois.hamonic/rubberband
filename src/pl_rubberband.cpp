#include <math.h>
#include <map>
#include <iostream>

#include "osiclpsolver_builder.hpp"

#include "lemon/list_graph.h"
#include "lemon/adaptors.h"
#include "lemon/dim2.h"
#include "lemon/graph_to_eps.h"

#include "coin/CbcModel.hpp"
#include "coin/OsiClpSolverInterface.hpp"
#include "coin/CoinPackedMatrix.hpp"

typedef lemon::ListGraph Graph_t;
typedef lemon::dim2::Point<double> Point_t;

Graph_t graph;
int n;
int m;
Graph_t::EdgeMap<double> strengthMap(graph);
std::map<Graph_t::Node, Point_t> nailed_nodes;


void create_nodes(int n) {
    for(int i=0; i<n; i++)
        graph.addNode();
}
void create_edge(int u, int v, double strength) {
    Graph_t::Edge e = graph.addEdge(graph.nodeFromId(u), graph.nodeFromId(v));
    strengthMap[e] = strength;
}
void parse(std::string & file_name) {
    std::ifstream in(file_name);
    int s;
    in >> n >> m >> s;

    assert(n > s && m > 1 && s > 2);

    create_nodes(n);
    for(int i=0; i<s; i++) {
        int v;
        double x, y;
        in >> v >> x >> y;
        nailed_nodes[graph.nodeFromId(v)] = Point_t(x, y);
    }
    for(int i=0; i<m; i++) {
        int u, v;
        double strength;
        in >> u >> v >> strength;
        create_edge(u, v, strength);
    }
}
std::string getFileName(const std::string& s) {
   char sep = '/';
   size_t i = s.rfind(sep, s.length());
   if (i != std::string::npos) {
      return(s.substr(i+1, s.length() - i));
   }
   return("");
}

int main (int argc, const char *argv[]) {
    if(argc < 3) {
        std::cerr << "input requiered : <graph data file> <g>" << std::endl;
        return -1;
    }
    std::string file_name = argv[1];
    const int g = std::atoi(argv[2]);

    parse(file_name);

    double max_d2 = 0;
    for( auto const& [_, p1] : nailed_nodes ) {
        for( auto const& [_, p2] : nailed_nodes ) {
            const double d2 = std::pow(p2.x*p1.x, 2) + std::pow(p2.y-p1.y, 2);
            max_d2 = std::max(d2, max_d2);
        }
    }
    const double diameter = std::sqrt(max_d2);




    auto x_var = [&] (Graph_t::Node v) {
        return graph.id(v);
    };
    const int x_var_number = n;
    
    const int y_offset = x_var_number;
    auto y_var = [&] (Graph_t::Node v) {
        return y_offset + graph.id(v);
    };
    const int y_var_number = n;

    const int x2_offset = y_offset + y_var_number;
    auto x2_var = [&] (Graph_t::Edge e) {
        return x2_offset + graph.id(e);
    };
    const int x2_var_number = m;

    const int y2_offset = x2_offset + x2_var_number;
    auto y2_var = [&] (Graph_t::Edge e) {
        return y2_offset + graph.id(e);
    };
    const int y2_var_number = m;




    auto m_const = [&] (Graph_t::Edge e, int i) {
        (void)e;
        return diameter*i/(double)(g-1);
    };
    auto o_const = [&] (Graph_t::Edge e, int i) {
        return strengthMap[e] * pow(m_const(e,i), 2);
    };
 

    OsiClpSolver_Builder * solver_builder = new OsiClpSolver_Builder();

    solver_builder->addVarType(x_var_number, NULL, NULL, -solver_builder->infty(), solver_builder->infty());
    solver_builder->addVarType(y_var_number, NULL, NULL, -solver_builder->infty(), solver_builder->infty());
    solver_builder->addVarType(x2_var_number, NULL, NULL, 0, solver_builder->infty());
    solver_builder->addVarType(y2_var_number, NULL, NULL, 0, solver_builder->infty());
    solver_builder->init();

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    // Columns
    for(Graph_t::EdgeIt e(graph); e != lemon::INVALID; ++e) {
        solver_builder->setObjective(x2_var(e), 1);
        solver_builder->setObjective(y2_var(e), 1);
    }
    // Rows
    // flow constraints
    // power loss constraint
    for(Graph_t::EdgeIt e(graph); e != lemon::INVALID; ++e) {
        Graph_t::Node u = graph.u(e);
        Graph_t::Node v = graph.v(e);
        
        for(int i = 0; i < g; i++) {
            const double scale = (o_const(e,i+1) - o_const(e,i)) / (m_const(e,i+1) - m_const(e,i));
            const double offset = o_const(e,i) - scale * m_const(e,i);

            // x2_e >= scale * (x_u - x_v) + offset
            solver_builder->buffEntry(x2_var(e), 1);
            solver_builder->buffEntry(x_var(u), -scale);
            solver_builder->buffEntry(x_var(v), scale);
            solver_builder->pushRow(offset, solver_builder->infty());

            solver_builder->buffEntry(x2_var(e), 1);
            solver_builder->buffEntry(x_var(u), scale);
            solver_builder->buffEntry(x_var(v), -scale);
            solver_builder->pushRow(offset, solver_builder->infty());


            solver_builder->buffEntry(y2_var(e), 1);
            solver_builder->buffEntry(y_var(u), -scale);
            solver_builder->buffEntry(y_var(v), scale);
            solver_builder->pushRow(offset, solver_builder->infty());

            solver_builder->buffEntry(y2_var(e), 1);
            solver_builder->buffEntry(y_var(u), scale);
            solver_builder->buffEntry(y_var(v), -scale);
            solver_builder->pushRow(offset, solver_builder->infty());
        }
    }
    // nailed nodes
    for( auto const& [u, p] : nailed_nodes ) {
        solver_builder->setBounds(x_var(u), p.x, p.x);
        solver_builder->setBounds(y_var(u), p.y, p.y);
    }
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    
    OsiClpSolverInterface * solver = solver_builder->buildSolver(OsiClpSolver_Builder::MIN);

    // solver->writeLp("debug.lp");

    solver->resolve();
    const double * solution = solver->getColSolution();


    Graph_t::NodeMap<Point_t> coordsMap(graph);
    for(Graph_t::NodeIt u(graph); u != lemon::INVALID; ++u) {           
        const double x_u = solution[x_var(u)];           
        const double y_u = solution[y_var(u)];
        coordsMap[u] = Point_t(x_u, y_u);
    }

    // eps output
    std::string output = "output/" + getFileName(file_name) + ".eps";
    lemon::graphToEps(graph, output).coords(coordsMap).run();
    // open eps file
    std::string command = "xdg-open " + output;
    if(std::system(command.c_str()) != 0)
        std::cerr << "failed to launch \"" << command << "\" , open it manually, cheers." << std::endl;

    // python output
    std::cout << "[";
    Point_t & p = coordsMap[graph.nodeFromId(0)];
    std::cout << "[" << p.x << ", " << p.y << ", 0]";
    for(int i=1; i<n; i++) {
        p = coordsMap[graph.nodeFromId(i)];
        std::cout << ",[" << p.x << ", " << p.y << ", 0]";
    }
    std::cout << "]" << std::endl << "[";

    delete solver;
    delete solver_builder;

    return 0;
}   