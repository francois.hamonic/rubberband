import math

center_x = 0
center_y = 0
radius = 1
nb_vertices = 5
angle = 2 * math.pi / nb_vertices

for i in range(nb_vertices):
    x = center_x + radius * math.sin(i * angle)
    y = center_y + radius * math.cos(i * angle)