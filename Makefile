CC=g++-9
CC_NORM=c++17

INCLUDE_DIR=include

SRC_DIR=src
BUILD_DIR=build


EIGEN_INCLUDE_DIR=~/Libs/eigen-eigen-323c052e1731/
COINOR_INCLUDE_DIR=~/Libs/coinor/dist/include/
COINOR_LIB_PATH=~/Libs/coinor/dist/lib/
LEMON_INCLUDE_DIR=~/Libs/lemon-1.3.1/


CFLAGS=-g -W -Wall -ansi -pedantic -std=$(CC_NORM) -fconcepts -O2 -flto -march=native -pipe -I $(INCLUDE_DIR) -I $(SRC_DIR) -I $(EIGEN_INCLUDE_DIR) -I $(COINOR_INCLUDE_DIR) -I $(LEMON_INCLUDE_DIR) -L $(COINOR_LIB_PATH)
LDFLAGS=-L $(COINOR_LIB_PATH) -lClp -lCbc -lCoinUtils -lOsiClp -pthread -lemon
EXEC=pl_rubberband
EXTENSION=.out

SRC:=
OBJ=$(addprefix $(BUILD_DIR)/,$(SRC:.cpp=.o))

build_dir: 
	mkdir -p $(BUILD_DIR)

all : build_dir $(OBJ) $(EXEC)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) -o $@ -c $< $(CFLAGS)


pl_rubberband: $(OBJ) $(BUILD_DIR)/pl_rubberband.o
	$(CC) -o $@$(EXTENSION) $^ $(LDFLAGS)

	
clean:
	rm -rf $(BUILD_DIR)/*.o

mrproper: clean
	rm -rf $(BUILD_DIR)
	rm -rf -f $(EXEC:=$(EXTENSION))